<?php

namespace App\Payment\Providers;


class DataProviderY extends Provider
{

    public $id;
    public $name;
    public $email;
    public $status;
    public $currency;
    public $balance;
    public $created_at;



    public function __construct($data)
    {
        if (is_array($data)) {
            return $this->createFromArray($data);
        }

        return $this->createFromObject($data);
    }

    protected function createFromObject($object)
    {


        $this->id = $object->id;
        $this->email = $object->email;
        $this->status = $object->status;
        $this->currency = $object->currency;
        $this->balance = $object->balance;
        $this->created_at = $object->created_at;
        return $this;
    }


    protected function createFromArray($array)
    {

        $this->id = $array['id'];
        $this->email = $array['email'];
        $this->status = $array['status'];
        $this->currency = $array['currency'];
        $this->balance = $array['balance'];
        $this->created_at = $array['created_at'];
        return $this;
    }
    public static function createFrom($data)
    {
        return new static($data);
    }

    public function getPaymentId()
    {
        return $this->id;
    }


    public function getPaymentBalance()
    {
        return $this->balance;
    }


    public function getPaymentCurrency()
    {
        return $this->currency;
    }


    public function getPaymentStatus()
    {
        return $this->status;
    }


    public function getPaymentEmail()
    {
        return $this->email;
    }

    public function getPaymentCreatedAt()
    {
        return $this->created_at;
    }
}
