<?php

namespace App\Payment\Providers;

Abstract Class Provider
{
   
    abstract public function getPaymentId();

  
    abstract public function getPaymentBalance();

 
    abstract public function getPaymentCurrency();

 
    abstract public function getPaymentStatus();


    abstract public function getPaymentEmail();

    abstract public function getPaymentCreatedAt();

    public function toArray()
    {
       
        $addressData = [
            'id' => $this->getPaymentId(),
            'balance' => $this->getPaymentBalance(),
            'currency' => $this->getPaymentCurrency(),
            'email' => $this->getPaymentEmail(),
            'status' => $this->getPaymentStatus(),
            'created_at' => $this->getPaymentCreatedAt()
     
        ];


        return $addressData;
    }

}