<?php

namespace App\Payment\Providers;


class DataProviderX extends Provider
{

    public $id;
    public $name;
    public $email;
    public $status;
    public $currency;
    public $balance;
    public $created_at;
   
 
    
    public function __construct($data)
    {
        if (is_array($data)) {
            return $this->createFromArray($data);
        }

        return $this->createFromObject($data);
    }

    protected function createFromObject($object)
    {
    
 
        $this->id = $object->parentIdentification;
        $this->email = $object->parentEmail;
        $this->status = $object->statusCode;
        $this->currency = $object->Currency;
        $this->balance = $object->parentAmount;
        $this->created_at = $object->registerationDate;
        return $this;
    }

  
    protected function createFromArray($array)
    {
   
        $this->id = $array['parentIdentification'];
        $this->email = $array['parentEmail'];
        $this->status = $array['statusCode'];
        $this->currency = $array['Currency'];
        $this->balance = $array['parentAmount'];
        $this->created_at = $array['registerationDate'];
        return $this;
    }
    public static function createFrom($data)
    {
        return new static($data);
    }
     public function getPaymentId(){
         return $this->id;
     }

  
     public function getPaymentBalance(){
        return $this->balance;
     }

 
     public function getPaymentCurrency(){
        return $this->currency;
     }

 
     public function getPaymentStatus(){
        return $this->status;
     }


     public function getPaymentEmail(){
        return $this->email;
     }
     public function getPaymentCreatedAt(){
        return $this->created_at;
     }

   
}
