<?php

namespace App\Enum;
use App\Abstracts\Enum;

class DataProviderYStatus extends Enum{

    const AUTHORISED = 100;
    const DECLINE = 200;
    const REFUNDED = 300;

}
