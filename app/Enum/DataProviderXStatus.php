<?php

namespace App\Enum;
use App\Abstracts\Enum;

class DataProviderXStatus extends Enum{

    const AUTHORISED = 1;
    const DECLINE = 2;
    const REFUNDED = 3;

}
