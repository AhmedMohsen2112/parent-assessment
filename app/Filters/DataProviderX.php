<?php

namespace App\Filters;

use App\Abstracts\CollectionFilters;
use App\Enum\DataProviderXStatus;

class DataProviderX extends CollectionFilters
{

    public function __construct($params = array())
    {
        parent::__construct($params);
    }

  

    public function currency($term)
    {
      
        return $this->collection->where("Currency", $term);
    }
    public function balanceMin($term)
    {
      
        return $this->collection->where("parentAmount",'>=', $term);
    }
    public function balanceMax($term)
    {
      
        return $this->collection->where("parentAmount",'<=', $term);
    }
    public function statusCode($term)
    {
      
        return $this->collection->where("statusCode", DataProviderXStatus::searchByKey($term));
    }
    

   
}
