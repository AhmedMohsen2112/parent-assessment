<?php

namespace App\Filters;

use App\Abstracts\CollectionFilters;
use App\Enum\DataProviderYStatus;

class DataProviderY extends CollectionFilters
{

    public function __construct($params = array())
    {
        parent::__construct($params);
    }

  

    public function currency($term)
    {
      
        return $this->collection->where("currency", $term);
    }
    public function balanceMin($term)
    {
      
        return $this->collection->where("balance",'>=', $term);
    }
    public function balanceMax($term)
    {
      
        return $this->collection->where("balance",'<=', $term);
    }
    public function statusCode($term)
    {
      
        return $this->collection->where("status", DataProviderYStatus::searchByKey($term));
    }
    

   
}
