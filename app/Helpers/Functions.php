<?php


if (!function_exists('t')) {

    function t($string, $replace = [], $file = 'app', $locale = null)
    {
        $item = $file . '.' . $string;

        if (Lang::has($item)) {
            if (!is_null($locale)) {
                $locale = $locale;
            }
            $line = trans($file . '.' . $string, $replace, $locale);
        } else {
            $line =  ucwords(str_replace('_', ' ', $string));
        }
        return $line;
    }
}