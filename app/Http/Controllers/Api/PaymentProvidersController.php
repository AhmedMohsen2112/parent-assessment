<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use JsonMachine\JsonMachine;
use App\Payment\Providers\DataProviderY as DataProviderYProvider;
use App\Payment\Providers\DataProviderX as DataProviderXProvider;

class PaymentProvidersController extends ApiController
{




    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        try {
            ini_set('max_execution_time', 300);


            $provider = $request->input('provider');

            //provider path
            $path = base_path(sprintf('app/Data/%s.json', $provider));

            //check if provider file exist
            if ($provider && !file_exists($path)) {
                return not_found('provider is not found');
            } else if ($provider && file_exists($path)) {
                $files = [$path];
            } else {
                $files = glob(base_path('app/Data/*.json'));
            }
            $data = [];
            foreach ($files as $file) {
                $data = array_merge($data, $this->parseJson(basename($file, ".json"), $file, $request->all()));
            }
            // dd($data);
            return ok($data);
        } catch (\Exception $ex) {
            dd($ex);
            return bad_request();
        }
    }

    private function parseJson($provider, $path, $filter)
    {
        $fileSize = filesize($path);
        $data = JsonMachine::fromFile($path);
        $data = collect(iterator_to_array($data));
        $filterClassName = "App\\Filters\\$provider";
        $filter = new $filterClassName($filter);
        $data = $filter->apply($data);

        foreach ($data as $key => $value) {
            // echo 'Progress: ' . intval($data->getPosition() / $fileSize * 100) . ' %';
            $transformer = "App\\Payment\\Providers\\$provider";
            $data[$key] = $transformer::createFrom($value)->toArray();
        }
        return $data->toArray();
    }
}
