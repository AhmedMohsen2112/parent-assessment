<?php

namespace App\Abstracts;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;

abstract class CollectionFilters
{

    protected $params;
    protected $collection;

    public function __construct($params = array())
    {
        $this->params = $params;
    }

    public function apply(Collection $collection)
    {
        $this->collection = $collection;
        foreach ($this->filters() as $name => $value) {
            if (!method_exists($this, $name)) {
                continue;
            }
            if (is_array($value)) {
                $this->collection=$this->$name($value);
            } else {
                if (strlen($value)) {
                    $this->collection=$this->$name($value);
                }
            }
        }
       
        return $this->collection;
    }

    public function filters()
    {
        return $this->params;
    }
}
